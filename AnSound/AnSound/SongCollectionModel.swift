//
//  SongCollection.swift
//  AnSound
//
//  Created by Mac on 28.06.2021.
//

import Foundation

struct SongCollectionModel: Decodable {
    var collection: [SongModel]
}
