//
//  PlayerConfigurator.swift
//  AnSound
//
//  Created by Mac on 21.07.2021.
//

import Foundation

class PlayerConfigurator {
    
    static let shared = PlayerConfigurator()
    
    func configure(playerController: PlayerController) {
        playerController.playlistController = PlaylistController()
        playerController.nowPlayingInfoController = NowPlayingInfoCenterController()
        playerController.nowPlayingInfoController.playerController = playerController
    }
    
}
