//
//  AppDelegate.swift
//  AnSound
//
//  Created by Mac on 17.06.2021.
//

import UIKit
import AVFoundation
import CoreData

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    public static var playerController = PlayerController.shared
    public static var miniplayervc = MiniPlayerViewController.storyboardInstance()
    
    //public static var container: NSPersistentContainer!
    /*
    func createContainer(completion: @escaping (NSPersistentContainer) -> ()) {
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { _, error in
            guard error != nil else {
                fatalError()
            }
            DispatchQueue.main.async {
                completion(container)
            }
        })
    }*/
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        /*createContainer { (container) in
            AppDelegate.container = container
        }*/

        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.playback, mode: .default, options: [])
        } catch {
            print("<AppDelegate>: <application didFinish>: Failed to set audio session category")
        }
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

