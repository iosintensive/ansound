//
//  PlayerModeles.swift
//  AnSound
//
//  Created by Mac on 22.07.2021.
//

import Foundation

enum PlayerModels {
    enum ButtonNames {
        static let play = "play.fill"
        static let pause = "pause.fill"
        static let heartTouched = "heart.fill"
        static let heart = "heart"
    }
}
