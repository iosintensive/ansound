//
//  LoginModel.swift
//  AnSound
//
//  Created by Mac on 24.06.2021.
//

import Foundation

struct LoginModel: Codable {
    var clientId: String
    var clientSecret: String
    var username: String
    var password: String
    //let scope = "non-expiring"
    //let grantType = "password"
    
    private enum CodingKeys: String, CodingKey {
        case clientId = "client_id"
        case clientSecret = "client_secret"
        case username = "username"
        case password = "password"
        //case scope = "scope"
        //case grantType = "grant_type"
    }
    
    init(clientId: String, clientSecret: String, username: String, password: String) {
        self.clientId = clientId
        self.clientSecret = clientSecret
        self.username = username
        self.password = password
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.clientId = try container.decode(String.self, forKey: .clientId)
        self.clientSecret = try container.decode(String.self, forKey: .clientSecret)
        self.username = try container.decode(String.self, forKey: .username)
        self.password = try container.decode(String.self, forKey: .password)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(clientId, forKey: .clientId)
        try container.encode(clientSecret, forKey: .clientSecret)
        try container.encode(username, forKey: .username)
        try container.encode(password, forKey: .password)
        //try container.encode(scope, forKey: .scope)
        //try container.encode(grantType, forKey: .grantType)
    }
}
