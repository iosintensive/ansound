//
//  MaxiPlayerPresenter.swift
//  AnSound
//
//  Created by Mac on 08.07.2021.
//

import Foundation

protocol MaxiPlayerPresentationLogic: class {
    func presentSongProgress(response: MaxiPlayer.DisplayProgress.Response)
    func presentSongData(response: MaxiPlayer.DisplayProgress.Response)
    func presentPlayPauseButton(response: MaxiPlayer.DisplayPlayPause.Response)
    func presentComment(response: MaxiPlayer.DisplayComment.Response)
    func presentFavorite(response: MaxiPlayer.DisplayFavorite.Response)
    func presentSelectPlaylistAlert(response: MaxiPlayer.DisplaySelectPlaylistAlert.Response)
    //func presentForward(response: MaxiPlayer.DisplayForward.Response)
    //func presentBackward(response: MaxiPlayer.DisplayBackward.Response)
    func presentSongData(response: MaxiPlayer.DisplaySongData.Response)
}

final class MaxiPlayerPresenter: MaxiPlayerPresentationLogic {
    
    weak var viewController: MaxiPlayerDisplayLogic?
    
    func presentSongProgress(response: MaxiPlayer.DisplayProgress.Response) {
        let viewModel = MaxiPlayer.DisplayProgress.ViewModel(
            progress: response.currentTime/response.duration,
            timeDuration: (Int(response.duration) / 60, Int(response.duration) % 60),
            time: (Int(response.currentTime) / 60, Int(response.currentTime) % 60))
        viewController?.displaySongProgress(viewModel: viewModel)
       
    }
    
    func presentSongData(response: MaxiPlayer.DisplayProgress.Response) {
        let viewModel = MaxiPlayer.DisplayProgress.ViewModel(
            progress: response.currentTime/response.duration,
            timeDuration: (Int(response.duration) / 60, Int(response.duration) % 60),
            time: (Int(response.currentTime) / 60, Int(response.currentTime) % 60))
        viewController?.displayTimeLabels(viewModel: viewModel)
    }
    
    func presentPlayPauseButton(response: MaxiPlayer.DisplayPlayPause.Response) {
        let viewModel = MaxiPlayer.DisplayPlayPause.ViewModel(playerIsPlaying: response.playerIsPlaying)
        viewController?.displayPlayPauseButton(viewModel: viewModel)
    }
    
    func presentComment(response: MaxiPlayer.DisplayComment.Response) {
        let viewModel = MaxiPlayer.DisplayComment.ViewModel(
            username: response.username,
            message: response.message)
        viewController?.displayComment(viewModel: viewModel)
    }
    
    func presentFavorite(response: MaxiPlayer.DisplayFavorite.Response) {
        let viewModel = MaxiPlayer.DisplayFavorite.ViewModel(
            isFavorite: response.isFavorite)
        viewController?.displayFavoriteButton(viewModel: viewModel)
    }
    
    func presentSelectPlaylistAlert(response: MaxiPlayer.DisplaySelectPlaylistAlert.Response) {
        let viewModel = MaxiPlayer.DisplaySelectPlaylistAlert.ViewModel(
            playlistsNames: response.playlistsNames,
            playlistsTypes: response.playlistsTypes)
        viewController?.displaySelectPlaylistAlert(viewModel: viewModel)
    }
    
    func presentSongData(response: MaxiPlayer.DisplaySongData.Response) {
        let viewModel = MaxiPlayer.DisplaySongData.ViewModel(
            artwork: response.artwork,
            title: response.title,
            author: response.author)
        viewController?.displaySongData(viewModel: viewModel)
    }
}
