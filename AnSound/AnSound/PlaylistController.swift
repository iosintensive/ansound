//
//  PlaylistController.swift
//  AnSound
//
//  Created by Mac on 15.07.2021.
//

import Foundation

enum PlaylistType {
    case search
    case favorites
    case others(Int)
}

class PlaylistController {
    
    var currentPlaylist: PlaylistModel!
    
    private var searchPlaylist: PlaylistModel!
    private var favoritesPlaylist: PlaylistModel!
    private var othersPlaylists: [PlaylistModel]!
    
    var count: Int {
        return othersPlaylists.count + 1
    }
    
    init() {
        loadPlaylists()
        currentPlaylist = searchPlaylist
    }
    
    func loadPlaylists() {
        self.searchPlaylist = PlaylistModel()
        self.favoritesPlaylist = PlaylistModel(name: "Favorites")
        self.othersPlaylists = [PlaylistModel(name: "For walking"), PlaylistModel(name: "For coding")]
    }
    
    func createNewOtherPlaylist(name: String) {
        let playlist = PlaylistModel(name: name)
        self.othersPlaylists.append(playlist)
    }
    
    func getOthers() -> [PlaylistModel] {
        return othersPlaylists
    }
    
    func getOthersPlaylistsNames() -> [String] {
        var playlistsNames: [String] = []
        
        for playlist in othersPlaylists {
            playlistsNames.append(playlist.name)
        }
        
        return playlistsNames
    }
    
    func getPlaylist(type: PlaylistType) -> PlaylistModel {
        switch (type) {
        case .search:
            return self.searchPlaylist
        case .favorites:
            return self.favoritesPlaylist
        case .others(let index):
            if checkIndex(index: index) {
                return self.othersPlaylists[index]
            }
        }
        //??
        return searchPlaylist
    }
    
    func selectPlaylist(type: PlaylistType) {
        switch(type) {
        case .search:
            self.currentPlaylist = searchPlaylist
        case .favorites:
            self.currentPlaylist = favoritesPlaylist
        case .others(let index):
            if checkIndex(index: index) {
                self.currentPlaylist = othersPlaylists[index]
            }
        }
    }
    
    func findSongInPlaylist(song: SongModel, type: PlaylistType) -> Bool {
        switch type {
        case .favorites:
            return self.favoritesPlaylist.find(song: song)
        case .others(let index):
            if checkIndex(index: index) {
                return self.othersPlaylists[index].find(song: song)
            }
        default:
            return false
        }
        return false
    }
    
    func addSongToPlaylist(song: SongModel, type: PlaylistType) {
        switch type {
        case .favorites:
            self.favoritesPlaylist.addSong(song: song)
        case .others(let index):
            if checkIndex(index: index) {
                self.othersPlaylists[index].addSong(song: song)
            }
        default:
            break
        }
    }
    
    func removeSongFromPlaylist(song: SongModel, type: PlaylistType) {
        switch type {
        case .favorites:
            self.favoritesPlaylist.removeSong(song: song)
        case .others(let index):
            self.othersPlaylists[index].removeSong(song: song)
        default:
            break
        }
    }
    
    func checkIndex(index: Int) -> Bool {
        if index < othersPlaylists.count && index >= 0 {
            return true
        }
        return false
    }
}
