//
//  FavoritesPlaylistViewController.swift
//  AnSound
//
//  Created by Mac on 14.07.2021.
//

import UIKit
import CoreData

class FavoritesPlaylistViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    

    @IBOutlet weak var playlistCollectionView: UICollectionView!
    
    var playlists: [String] = ["1", "2", "3"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playlistCollectionView.delegate = self
        playlistCollectionView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playlists.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: playlistCollectionView.bounds.width, height: playlistCollectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = playlistCollectionView.dequeueReusableCell(withReuseIdentifier: "PlaylistCollectionViewCell", for: indexPath) as! PlaylistCollectionViewCell
        
        
        //print("init cell")
        cell.playlist = PlaylistModel()
        cell.addElements()
        cell.addElements()
        return cell
    }
}
