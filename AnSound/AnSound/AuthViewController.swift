//
//  ViewController.swift
//  AnSound
//
//  Created by Mac on 17.06.2021.
//

import UIKit

class AuthViewController: UIViewController {
  
    
    @IBOutlet weak var LoginTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    static func storyboardInstance() -> AuthViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? AuthViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGradient()

        LoginTextField.layer.borderWidth = 1.0
        LoginTextField.layer.cornerRadius = 5.0
        LoginTextField.layer.borderColor = UIColor.darkGray.cgColor
        
        PasswordTextField.layer.borderWidth = 1.0
        PasswordTextField.layer.cornerRadius = 5.0
        PasswordTextField.layer.borderColor = UIColor.darkGray.cgColor
        // Do any additional setup after loading the view.
    }

    private func setupGradient() {
        let gradientLayer = CAGradientLayer()
        let blackColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        gradientLayer.colors = [UIColor.init(named: "BusinessColor")!.cgColor, blackColor.cgColor]
        
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 0.5)
        
        gradientLayer.frame = self.view.layer.bounds
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    @IBAction func LoginButtonDidTouch(_ sender: UIButton) {
        //let userData = LoginModel(clientId: APISoundCloudConstants.Keys.clientID, clientSecret: APISoundCloudConstants.Keys.clientSecret, username: LoginTextField.text!, password: PasswordTextField.text!)
        let userData = LoginModel(clientId: APISoundCloudConstants.Keys.clientID, clientSecret: APISoundCloudConstants.Keys.clientSecret, username: "dimkintron@mail.ru", password: "D1marik112rusb2")
        
        APISoundCloud.shared.loginRequest(userData: userData)
    }

}

