//
//  NowPlayingInfoCenterController.swift
//  AnSound
//
//  Created by Mac on 21.07.2021.
//

import Foundation
import AVFoundation

import AVKit
import MediaPlayer

class NowPlayingInfoCenterController {
    
    weak var playerController: PlayerControllerBusinessLogic?
    
    init () {
        configure()
    }
    
    private func configure() {
        setupCommandCenter()
    }
    
    private func setupCommandCenter() {
        UIApplication.shared.beginReceivingRemoteControlEvents()

        MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle: "AnSound"]
        
        let commandCenter = MPRemoteCommandCenter.shared()
        
        commandCenter.playCommand.isEnabled = true
        commandCenter.pauseCommand.isEnabled = true
        commandCenter.nextTrackCommand.isEnabled = true
        commandCenter.previousTrackCommand.isEnabled = true
        
        commandCenter.playCommand.addTarget {
            [weak self] (event) -> MPRemoteCommandHandlerStatus in
            self?.playerController?.play()
            return .success
        }
        commandCenter.pauseCommand.addTarget {
            [weak self] (event) -> MPRemoteCommandHandlerStatus in
            self?.playerController?.pause()
            return .success
        }
        commandCenter.nextTrackCommand.addTarget {
            [weak self] (event) -> MPRemoteCommandHandlerStatus in
            self?.playerController?.forward()
            return .success
        }
        commandCenter.previousTrackCommand.addTarget {
            [weak self] (event) -> MPRemoteCommandHandlerStatus in
            self?.playerController?.backward()
            return .success
        }
    }
    
    func setInfo(with song: SongModel) {
        guard let artwork = song.artworkUrl else {
            print("<Player Controller>: <setInfo>: artwork is nil")
            return
        }
        
        UIImageView().downloaded(from: artwork) { [weak self, song] (data) in
            guard let image = UIImage(data: data) else {
                print("<closure UIImageView.downloaded>: <Player Controller>: <setInfo>: Bad data")
                return
            }
            self?.setInfoCommandCenter(song: song, image: image)
        }
    }
    
    private func setInfoCommandCenter(song: SongModel, image: UIImage) {
        let artwork = MPMediaItemArtwork(boundsSize: CGSize(width: 512, height: 512)) { [image] (sz) -> UIImage in
            return image
        }
        let songInfo = [
            MPMediaItemPropertyTitle: song.title,
            MPMediaItemPropertyArtist: song.username,
            MPMediaItemPropertyArtwork: artwork
        ] as [String : Any]
        MPNowPlayingInfoCenter.default().nowPlayingInfo = songInfo
    }
}
