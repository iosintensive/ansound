//
//  PlaylistViewController.swift
//  AnSound
//
//  Created by Mac on 22.07.2021.
//

import UIKit

class PlaylistViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var index: Int = 0
    
    var playlistData: PlaylistModel!
    
    @IBOutlet weak var playlistNameLabel: UILabel!
    
    @IBOutlet weak var playlistTableView: UITableView!
    
    static func storyboardInstance() -> PlaylistViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? PlaylistViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playlistTableView.delegate = self
        playlistTableView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        self.playlistTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlistData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SongTableViewCell") as! SongTableViewCell
        
        let index = indexPath.row
        
        print("index \(index) \(playlistData.collection.collection[index].title)")
        
        if let song = playlistData[index] {
            cell.songNameLabel.text = song.title
            cell.authorNameLabel.text = song.username
            
            /*if let image = imagesCollection[index] {
                cell.songImageView.image = image
                print("SearchViewController: TabelView: Image was loaded from imagesCollection")
            }*/
            if let imageUrl = song.artworkUrl {
                cell.songImageView.downloaded(from: imageUrl) { [weak self, index] data in
                    guard let strongSelf = self else {
                        return
                    }
                    //strongSelf.imagesCollection[index] = UIImage(data: data)
                }
            }
        }
        else {
            print("Error loading cell data")
        }
    
        return cell
    }
}
