//
//  MaxiPlayerConfigurator.swift
//  AnSound
//
//  Created by Mac on 08.07.2021.
//

import Foundation



class MaxiPlayerConfigurator {
    
    static let shared = MaxiPlayerConfigurator()
    
    private init() { }
    
    func configure(_ controller: MaxiPlayerViewController) {
        let viewController = controller
        let interactor = MaxiPlayerInteractor()
        let presenter = MaxiPlayerPresenter()
        viewController.interactor = interactor
        interactor.presenter = presenter
        presenter.viewController = viewController
        
        interactor.playerController = PlayerController.shared
        interactor.configure()
    }
}
