//
//  SongModel.swift
//  AnSound
//
//  Created by Mac on 28.06.2021.
//

/*
 {
             "kind": "track",
             "id": 1062578740,
             "created_at": "2021/06/05 15:41:29 +0000",
             "duration": 134792,
             "commentable": true,
             "comment_count": 16,
             "sharing": "public",
             "tag_list": " ",
             "streamable": true,
             "embeddable_by": "all",
             "purchase_url": null,
             "purchase_title": null,
             "genre": "",
             "title": "Я в моменте",
             "description": "",
             "label_name": null,
             "release": null,
             "key_signature": null,
             "isrc": null,
             "bpm": null,
             "release_year": null,
             "release_month": null,
             "release_day": null,
             "license": "all-rights-reserved",
             "uri": "https://api.soundcloud.com/tracks/1062578740",
             "user": {
                 "avatar_url": "https://i1.sndcdn.com/avatars-exaIAxRYbgJG2Cx0-N7IL5w-large.jpg",
                 "id": 786161278,
                 "kind": "user",
                 "permalink_url": "https://soundcloud.com/korolevskaya-musica2",
                 "uri": "https://api.soundcloud.com/users/786161278",
                 "username": "Королевская музыка",
                 "permalink": "korolevskaya-musica2",
                 "created_at": "2020/02/26 17:38:53 +0000",
                 "last_modified": "2021/05/28 16:37:13 +0000",
                 "first_name": "",
                 "last_name": "",
                 "full_name": "",
                 "city": "",
                 "description": "✍ По-поводу рекламы (репост, добавить в плейлист, опубликовать, закрепить, подписаться, лайкнуть) писать в IG или напрямую в SC.\n💳 Подписывайтесь на второй аккаунт ниже.",
                 "country": null,
                 "track_count": 67,
                 "public_favorites_count": 0,
                 "reposts_count": 0,
                 "followers_count": 0,
                 "followings_count": 0,
                 "plan": "Free",
                 "myspace_name": null,
                 "discogs_name": null,
                 "website_title": null,
                 "website": null,
                 "comments_count": 0,
                 "online": false,
                 "likes_count": 0,
                 "playlist_count": 6,
                 "subscriptions": [
                     {
                         "product": {
                             "id": "free",
                             "name": "Free"
                         }
                     }
                 ]
             },
             "permalink_url": "https://soundcloud.com/korolevskaya-musica2/ya-v-moment",
             "artwork_url": "https://i1.sndcdn.com/artworks-sHEjoZ3HvAJiizQQ-60a38w-large.jpg",
             "stream_url": "https://api.soundcloud.com/tracks/1062578740/stream",
             "download_url": "https://api.soundcloud.com/tracks/1062578740/download",
             "waveform_url": "https://wave.sndcdn.com/wzEtKIBrb5Xm_m.png",
             "available_country_codes": null,
             "secret_uri": null,
             "user_favorite": null,
             "user_playback_count": null,
             "playback_count": 192231,
             "download_count": 0,
             "favoritings_count": 6020,
             "reposts_count": 45,
             "downloadable": false,
             "access": "playable",
             "policy": null,
             "monetization_model": null
         }
 */

import Foundation
import CoreData

struct SongModel: Decodable {
    let id: Int
    let title: String
    let artworkUrl: String?
    let downloadUrl: String
    let streamUrl: String
    let username: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case artworkUrl = "artwork_url"
        case downloadUrl = "download_url"
        case user
        case streamUrl = "stream_url"
    }
    enum UserCodingKeys: String, CodingKey {
        case username
    }
    
    
    init(id: Int, title: String, username: String) {
        self.id = id
        self.title = title
        self.username = username
        
        self.artworkUrl = nil
        self.downloadUrl = ""
        self.streamUrl = ""
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.title = try container.decode(String.self, forKey: .title)
        self.artworkUrl = try? container.decode(String.self, forKey: .artworkUrl)
        self.downloadUrl = try container.decode(String.self, forKey: .downloadUrl)
        self.streamUrl = try container.decode(String.self, forKey: .streamUrl)
        
        let user = try container.nestedContainer(keyedBy: UserCodingKeys.self, forKey: .user)
        self.username = try user.decode(String.self, forKey: .username)
    }
}
