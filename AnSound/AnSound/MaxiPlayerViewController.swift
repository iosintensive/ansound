//
//  MaxiPlayerViewController.swift
//  AnSound
//
//  Created by Mac on 08.07.2021.
//

import UIKit

protocol MaxiPlayerDisplayLogic: class {
    func displaySongProgress(viewModel: MaxiPlayer.DisplayProgress.ViewModel)
    func displayTimeLabels(viewModel: MaxiPlayer.DisplayProgress.ViewModel)
    func displayPlayPauseButton(viewModel: MaxiPlayer.DisplayPlayPause.ViewModel)
    func displayComment(viewModel: MaxiPlayer.DisplayComment.ViewModel)
    func displayFavoriteButton(viewModel: MaxiPlayer.DisplayFavorite.ViewModel)
    func displaySelectPlaylistAlert(viewModel: MaxiPlayer.DisplaySelectPlaylistAlert.ViewModel)
    func displaySongData(viewModel: MaxiPlayer.DisplaySongData.ViewModel)
}

class MaxiPlayerViewController: UIViewController, MaxiPlayerDisplayLogic {
    
    var interactor: MaxiPlayerBusinesLogic?
    
    var delegate: MiniPlayerDelegate?
    
    @IBOutlet weak var songSlider: UISlider!
    @IBOutlet weak var songImage: UIImageView!
    @IBOutlet weak var songTitle: UILabel!
    @IBOutlet weak var authorName: UILabel!
    
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var durationTimeLabel: UILabel!
    
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    
    @IBOutlet weak var commentUsernameLabel: UILabel!
    @IBOutlet weak var commentMessageLabel: UILabel!
    
    static func storyboardInstance() -> MaxiPlayerViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? MaxiPlayerViewController
    }

    override func awakeFromNib() {
        MaxiPlayerConfigurator.shared.configure(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        songImage.layer.cornerRadius = 5.0
        
        commentMessageLabel.alpha = 0.0
        commentUsernameLabel.alpha = 0.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let request = MaxiPlayer.DisplayState.Request()
        interactor?.setState(request: request)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.delegate?.updateMiniPlayerData()
    }
    
    deinit {
        interactor?.deconfigure()
    }
    
    func displaySongProgress(viewModel: MaxiPlayer.DisplayProgress.ViewModel) {
        self.displayTimeLabels(viewModel: viewModel)
        self.songSlider.setValue(viewModel.progress, animated: false)
    }
    
    func displayTimeLabels(viewModel: MaxiPlayer.DisplayProgress.ViewModel) {
        self.currentTimeLabel.text = String(format: "%01d:%02d", viewModel.time.0, viewModel.time.1)
        self.durationTimeLabel.text = String(format: "%01d:%02d", viewModel.timeDuration.0, viewModel.timeDuration.1)
    }
    func displayPlayPauseButton(viewModel: MaxiPlayer.DisplayPlayPause.ViewModel) {
        let options: UIView.AnimationOptions = [.curveEaseOut]
        if viewModel.playerIsPlaying {
            self.playPauseButton.setImage(UIImage(systemName: PlayerModels.ButtonNames.pause), for: .normal)
            UIView.animate(withDuration: 0.2, delay: 0.0, options: options) { [weak self] in
                self?.songImage.transform = CGAffineTransform(scaleX: 1.25, y: 1.25)
                
            }
        }
        else {
            self.playPauseButton.setImage(UIImage(systemName: PlayerModels.ButtonNames.play), for: .normal)
            UIView.animate(withDuration: 0.2, delay: 0.0, options: options) { [weak self] in
                self?.songImage.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
        }
    }
    
    func displayComment(viewModel: MaxiPlayer.DisplayComment.ViewModel) {
        DispatchQueue.main.async {
            self.commentUsernameLabel.text = viewModel.username
            self.commentMessageLabel.text = viewModel.message
            
            self.commentUsernameLabel.alpha = 1.0
            self.commentMessageLabel.alpha = 1.0
        }
    }
    
    func displayFavoriteButton(viewModel: MaxiPlayer.DisplayFavorite.ViewModel) {
        if viewModel.isFavorite {
            self.favoriteButton.setImage(UIImage(systemName: PlayerModels.ButtonNames.heartTouched), for: .normal)
        }
        else {
            self.favoriteButton.setImage(UIImage(systemName: PlayerModels.ButtonNames.heart), for: .normal)
        }
    }
    
    func displaySongData(viewModel: MaxiPlayer.DisplaySongData.ViewModel) {
        self.authorName.text = viewModel.author
        self.songTitle.text = viewModel.title
        
        guard let urlArtwork = viewModel.artwork else {
            print("<MaxiPlayerController>: <displaySongData>: artwork is nil")
            return
        }
        self.songImage.downloaded(from: urlArtwork) { (data) in
        }
    }
    
    func displaySelectPlaylistAlert(viewModel: MaxiPlayer.DisplaySelectPlaylistAlert.ViewModel) {
        let alert = UIAlertController(
            title: "Select the playlist",
            message: "Select the playlist in which you want to add the song",
            preferredStyle: .actionSheet)
        
        let titleAttributes = [
            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 22)!,
            NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        let titleString = NSAttributedString(
            string: "Add song to the playlist",
            attributes: titleAttributes)
        
        let messageAttributes = [
            NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 18)!,
            NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        let messageString = NSAttributedString(
            string: "Select the playlist in which you want to add the song",
            attributes: messageAttributes)
        alert.setValue(titleString, forKey: "attributedTitle")
        alert.setValue(messageString, forKey: "attributedMessage")
        alert.view.tintColor = .black
        var index = 0
        
        for name in viewModel.playlistsNames {
            let action = UIAlertAction(title: name, style: .default) { [weak self, index] (action) in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.interactor?.addSongToThePlaylist(type: .others(index))
            }
            alert.addAction(action)
            index += 1
        }
        let action = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alert.addAction(action)
        /*
        if let bgView = alert.view.subviews.first, let groupView = bgView.subviews.first, let contentView = groupView.subviews.first {
            contentView.backgroundColor = UIColor.init(red: 0.11, green: 0.11, blue: 0.12, alpha: 0)
        }*/

        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func playPauseButtonDidTouch(_ sender: Any) {
        let request = MaxiPlayer.DisplayPlayPause.Request()
        interactor?.playPausePlayer(request: request)
    }
    
    @IBAction func forwardButtonDidTouch(_ sender: Any) {
        let request = MaxiPlayer.DisplayForward.Request()
        interactor?.forwardPlayer(request: request)
    }
    
    @IBAction func backwardButtonDidTouch(_ sender: Any) {
        let request = MaxiPlayer.DisplayBackward.Request()
        interactor?.backwardPlayer(request: request)
    }
    
    @IBAction func favoriteButtonDidTouch(_ sender: Any) {
        let request = MaxiPlayer.DisplayFavorite.Request()
        interactor?.setFavorite(request: request)
    }
    
    @IBAction func addToPlaylistButtonDidTouch(_ sender: Any) {
        let request = MaxiPlayer.DisplaySelectPlaylistAlert.Request()
        interactor?.setupAlert(request: request)
    }
    
    @IBAction func songSliderValueChanged(_ sender: Any) {
        let request = MaxiPlayer.DisplayProgress.Request(value: songSlider.value)
        //interactor?.deconfigure()
        interactor?.chagedSliderValue(request: request)
    }
    
    @IBAction func songSliderTouchUpInside(_ sender: Any) {
        let request = MaxiPlayer.DisplayPlayPlayer.Requset()
        interactor?.playPlayer(request: request)
    }
    
    @IBAction func songSliderTouchDown(_ sender: Any) {
        let request = MaxiPlayer.DisplayPausePlayer.Request()
        interactor?.pausePlayer(request: request)
    }
}
