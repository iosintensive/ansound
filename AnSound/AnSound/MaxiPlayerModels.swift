//
//  MaxiPlayerModels.swift
//  AnSound
//
//  Created by Mac on 08.07.2021.
//

import Foundation

enum MaxiPlayer {
    enum DisplayProgress {
        struct Request {
            let value: Float
        }
        struct Response {
            let currentTime: Float
            let duration: Float
        }
        struct ViewModel {
            let progress: Float // [0, 1] range value
            let timeDuration: (Int, Int) //minutes, seconds
            let time: (Int, Int) //minutes, seconds
        }
    }
    enum DisplayPlayPause {
        struct Request {
        }
        struct Response {
            let playerIsPlaying: Bool
        }
        struct ViewModel {
            let playerIsPlaying: Bool
        }
    }
    enum DisplayPlayPlayer {
        struct Requset {
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
    enum DisplayComment {
        struct Response {
            let username: String
            let message: String
        }
        struct ViewModel {
            let username: String
            let message: String
        }
    }
    enum DisplayFavorite {
        struct Request {
        }
        struct Response{
            let isFavorite: Bool
        }
        struct ViewModel {
            let isFavorite: Bool
        }
    }
    enum DisplayPausePlayer {
        struct Request {
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
    
    enum DisplayState {
        struct Request {
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
    enum DisplaySelectPlaylistAlert {
        struct Request {
        }
        struct Response {
            let playlistsNames: [String]
            let playlistsTypes: [PlaylistType]
        }
        struct ViewModel {
            let playlistsNames: [String]
            let playlistsTypes: [PlaylistType]
        }
    }
    enum DisplayForward {
        struct Request {
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
    enum DisplayBackward {
        struct Request {
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
    enum DisplaySongData {
        struct Request {
        }
        struct Response {
            let artwork: String?
            let title: String
            let author: String
        }
        struct ViewModel {
            let artwork: String?
            let title: String
            let author: String
        }
    }
}
