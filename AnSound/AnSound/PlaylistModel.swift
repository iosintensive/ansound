//
//  PlaylistModel.swift
//  AnSound
//
//  Created by Mac on 05.07.2021.
//

import Foundation
enum PlaylistSettings {
    case changeCurrentIndex
    case defaultSettings
}

class PlaylistModel {
    var name: String
    
    var collection: SongCollectionModel {
        willSet {
            currentIndex = 0
        }
    }
    
    var currentIndex: Int = 0
    
    var count: Int {
        get {
            return collection.collection.count
        }
    }
    
    init(name: String){
        self.name = name
        self.collection = SongCollectionModel(collection: [])
    }
    
    init(){
        self.name = "unnamed"
        self.collection = SongCollectionModel(collection: [])
    }
    
    subscript(index: Int) -> SongModel?{
        get {
            if index >= 0 && index < collection.collection.count {
                return collection.collection[index]
            }
            else {
                return nil
            }
        }
        set(newValue) {
            if index >= 0 && index < collection.collection.count {
                guard let value = newValue else { return }
                collection.collection[index] = value
            }
        }
    }
    
    subscript(index: Int, with settings: PlaylistSettings) -> SongModel? {
        get {
            if settings == .changeCurrentIndex {
                    currentIndex = index
            }
            return self[index]
        }
        set(newValue) {
            if settings == .changeCurrentIndex {
                    currentIndex = index
            }
            self[index] = newValue
        }
    }
    func addSong(song: SongModel) {
        if !find(song: song) {
            self.collection.collection.append(song)
        }
    }
    
    func removeSong(song: SongModel) {
        if find(song: song) {
            self.collection.collection.removeAll(where: {$0.id == song.id})
        }
    }
    
    func find(song: SongModel) -> Bool {
        if self.collection.collection.contains(where: { $0.id == song.id }) {
            return true
        }
        return false
    }
    
    func backward() -> SongModel {
        if currentIndex - 1 >= 0 {
            currentIndex = currentIndex - 1
        }
        else {
            currentIndex = collection.collection.count - 1
        }
        return collection.collection[currentIndex]
    }
    
    func forward() -> SongModel {
        if currentIndex + 1 < collection.collection.count {
            currentIndex = currentIndex + 1
        }
        else {
            currentIndex = 0
        }
        return collection.collection[currentIndex]
    }
}
