//
//  PlaylistCollectionViewCell.swift
//  AnSound
//
//  Created by Mac on 14.07.2021.
//

import UIKit

class PlaylistCollectionViewCell: UICollectionViewCell, UITableViewDelegate, UITableViewDataSource {
    
    var rootView: FavoritesPlaylistViewController?

    var playlist: PlaylistModel!
    
    @IBOutlet weak var songTableViewCell: UITableView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.songTableViewCell.delegate = self
        self.songTableViewCell.dataSource = self
        
        
        //addElements()
    }
    
    func addElements() {
        playlist.collection.collection.append(
            SongModel(
                id: 0,
                title: "d1mka",
                username: "d1"))
        playlist.collection.collection.append(
            SongModel(
                id: 0,
                title: "slavnii knight",
                username: "anaar"))
        playlist.collection.collection.append(SongModel(
                id: 0,
                title: "oi oi oi",
                username: "doom"))
    }
    
    func updateTableView() {
        songTableViewCell.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlist.count
    }
    
    //TODO: add fetch data from PlaylistModel
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SongTableViewCell") as! SongTableViewCell
        
        cell.songNameLabel.text = playlist[indexPath.row]?.title
        cell.authorNameLabel.text = playlist[indexPath.row]?.username
        
        return cell
    }
}
