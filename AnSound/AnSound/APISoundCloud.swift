//
//  APISoundCloud.swift
//  AnSound
//
//  Created by Mac on 24.06.2021.
//

import Foundation

protocol APISoundCloudDelegate {
    func tracksWasFinded(collection: SongCollectionModel)
}

class APISoundCloud {
    
    public static let shared = APISoundCloud()
    
    public var delegate: APISoundCloudDelegate?
    
    func loginRequest(userData: LoginModel) {
        let data = try! JSONEncoder().encode(userData)
        
        guard let url = URL(string: APISoundCloudConstants.RequestUrl.oauth2) else {
            return
        }
        
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        print(String(data: data, encoding: .utf8)!)
       
        request.httpBody = data
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                return
            }
            print(String(data: data, encoding: .utf8)!)
        }
        
        task.resume()
    }
    func searchTracksWith(name: String) {
        let queryParams = [
            "q": name,
            "access": "",
            "limit": "20",
            "linked_partitioning": "true",
            "client_id": APISoundCloudConstants.Keys.clientID
        ]
        guard var components = URLComponents(string: APISoundCloudConstants.RequestUrl.searchTracks) else {
            return
        }
        components.queryItems = queryParams.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        
        var request = URLRequest(url: components.url!)
        
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                print("<APISoundCloud>: <searchTracksWith>: No data")
                return
            }
            //print(String(data: data, encoding: .utf8)!)
            
            let songs = try! JSONDecoder().decode(SongCollectionModel.self, from: data)

            //print(song.title)
            self.delegate?.tracksWasFinded(collection: songs)
        }
        
        task.resume()
    }
    func loadTrack(with urlString: String, complietion: @escaping (_ songData: Data) -> Void) {
        
        var urlComponents = URLComponents(string: urlString)
        urlComponents?.queryItems = [URLQueryItem(name: "client_id", value: APISoundCloudConstants.Keys.clientID)]
        
        guard let url = urlComponents?.url else {
            print("<APISoundCloud>: <loadTrack>: Bad url")
            return
        }

        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                print("<APISoundCloud>: <loadTrack>: No data")
                return
            }
            complietion(data)
        }
        task.resume()
    }
    
    func getTrackComments(with id: Int, complietion: @escaping (_ collection: TrackCommentsCollectionModel) -> Void) {
        let queryParams = [
            "limit": "100",
            "linked_partitioning": "true",
            "client_id": APISoundCloudConstants.Keys.clientID
        ]
        guard var components = URLComponents(string: APISoundCloudConstants.RequestUrl.trackComments(id: id)) else {
            return
        }
        components.queryItems = queryParams.map { (key, value) in
            URLQueryItem(name: key, value: value)
        }
        
        var request = URLRequest(url: components.url!)
        
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                print("<APISoundCloud>: <getTrackComments>: No data")
                return
            }
            let comments = try! JSONDecoder().decode(TrackCommentsCollectionModel.self, from: data)
            
            complietion(comments)
            
            //print(song.title)
            //self.delegate?.tracksWasFinded(collection: songs)
        }
        
        task.resume()
    }
    
    func addClientIdToTheUrl(stringUrl: String) -> URL? {
        var urlComponents = URLComponents(string: stringUrl)
        urlComponents?.queryItems = [URLQueryItem(name: "client_id", value: APISoundCloudConstants.Keys.clientID)]
        
        guard let url = urlComponents?.url else {
            print("<APISoundCloud>: <addClientIdToTheUrl>: Bad url")
            return nil
        }
        
        return url
    }
    
}
