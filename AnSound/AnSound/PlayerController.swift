//
//  PlayerController.swift
//  AnSound
//
//  Created by Mac on 08.07.2021.
//

import Foundation
import AVFoundation

import AVKit
import MediaPlayer

import CoreData

protocol PlayerControllerBusinessLogic: class {
    var currentSong: SongModel! {get set}
    func play()
    func pause()
    func forward()
    func backward()
}

class PlayerController: PlayerControllerBusinessLogic {
    
    static let shared = PlayerController()
    
    var player: AVPlayer!
    
    var playlistController: PlaylistController!
    
    var nowPlayingInfoController: NowPlayingInfoCenterController!
    
    //TODO: Fix the currentSong Image and currentSong models
    
    var currentSongImage: UIImage!
    var currentSong: SongModel!
    var isPlaying: Bool!
    
    init() {
        self.player = AVPlayer()
        
        PlayerConfigurator.shared.configure(playerController: self)
    }

    func addTimeObserver(handler: @escaping (CMTime)->Void) -> Any {
        let time = CMTime(seconds: 1.0, preferredTimescale: .max)
        return self.player.addPeriodicTimeObserver(forInterval: time, queue: nil, using: handler)
    }
    
    func removeTimeObserver(observer: Any?){
        if let token = observer {
            self.player.removeTimeObserver(token)
        }
    }
    
    func setSong(with song: SongModel) {
        guard let url = APISoundCloud.shared.addClientIdToTheUrl(stringUrl: song.streamUrl) else {
            print("<Player Controller>: <setSong>: Bad URL")
            return
        }
        let playerItem = AVPlayerItem(url: url)
        self.player.replaceCurrentItem(with: playerItem)
        
        self.currentSong = song
       
        self.nowPlayingInfoController.setInfo(with: song)
        
        self.play()
    }
    
    func setSongFromPlaylist(index: Int) {
        guard let song = playlistController.currentPlaylist[index, with: .changeCurrentIndex] else {
            print("<Player Controller>: <setSongFromPlaylist>: Wrong index")
            return
        }
        self.setSong(with: song)
        self.currentSong = song
    }
    
    func setValue(value: Float) {
        let time = CMTime(value: CMTimeValue(value), timescale: 1)
        self.player.seek(to: time)
    }
    
    func play() {
        isPlaying = true
        self.player.play()
    }
    
    func pause() {
        isPlaying = false
        self.player.pause()
    }
    
    func forward() {
        let song = self.playlistController.currentPlaylist.forward()
        self.setSong(with: song)
    }
    
    func backward() {
        let song = self.playlistController.currentPlaylist.backward()
        self.setSong(with: song)
    }
}
