//
//  APISoundCloudConstants.swift
//  AnSound
//
//  Created by Mac on 24.06.2021.
//

import Foundation

struct APISoundCloudConstants {
    
    struct Keys {
        static let clientID = "08cb4bc9efc7ebeb5945abe37ae11b39"
        static let clientSecret = "43d7b47398b1e57bf05a6f8ce0cc8a4"
    }
    
    struct RequestUrl {
        static let main = "https://api.soundcloud.com"
        static let connect = "https://api.soundcloud.com/connect"
        static let oauth2 = "https://api.soundcloud.com/oauth2/token"
        static let searchTracks = "https://api.soundcloud.com/tracks"
        static func trackComments(id: Int) -> String {
            return "https://api.soundcloud.com/tracks/\(id)/comments"
        }
    }
}
