//
//  TrackCommentsCollectionModel.swift
//  AnSound
//
//  Created by Mac on 13.07.2021.
//

import Foundation

class TrackCommentsCollectionModel: Decodable {
    var collection: [TrackCommentModel]
}
