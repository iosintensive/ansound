//
//  MaxiPlayerInteractor.swift
//  AnSound
//
//  Created by Mac on 08.07.2021.
//

import Foundation
import AVFoundation

protocol MaxiPlayerBusinesLogic: class {
    func chagedSliderValue(request: MaxiPlayer.DisplayProgress.Request)
    func playPausePlayer(request: MaxiPlayer.DisplayPlayPause.Request)
    func playPlayer(request: MaxiPlayer.DisplayPlayPlayer.Requset)
    func pausePlayer(request: MaxiPlayer.DisplayPausePlayer.Request)
    func setFavorite(request: MaxiPlayer.DisplayFavorite.Request)
    func setState(request: MaxiPlayer.DisplayState.Request)
    func setupAlert(request: MaxiPlayer.DisplaySelectPlaylistAlert.Request)
    func addSongToThePlaylist(type: PlaylistType)
    func forwardPlayer(request: MaxiPlayer.DisplayForward.Request)
    func backwardPlayer(request: MaxiPlayer.DisplayBackward.Request)
    func configure()
    func deconfigure()
}

final class MaxiPlayerInteractor: MaxiPlayerBusinesLogic {

    var presenter: MaxiPlayerPresentationLogic?
    
    var playerController = PlayerController.shared
    
    var timeObserver: Any?
    
    var isFavorite: Bool!
    
    var commentsCollection: TrackCommentsCollectionModel?
    
    func chagedSliderValue(request: MaxiPlayer.DisplayProgress.Request) {
        guard let item = playerController.player.currentItem else {
            print("<MaxiPlayerInteractor>: <configure>: no item in player")
            return
        }
        let value = request.value * Float(item.duration.seconds)
        playerController.setValue(value: value)
        
        let response = MaxiPlayer.DisplayProgress.Response(
            currentTime: value,
            duration: Float(item.duration.seconds))
        self.presenter?.presentSongData(response: response)
    }
    
    func playPausePlayer(request: MaxiPlayer.DisplayPlayPause.Request) {
        if self.playerController.isPlaying {
            self.playerController.pause()
        }
        else {
            self.playerController.play()
        }
        let response = MaxiPlayer.DisplayPlayPause.Response(playerIsPlaying: self.playerController.isPlaying)
        presenter?.presentPlayPauseButton(response: response)
    }
    
    func playPlayer(request: MaxiPlayer.DisplayPlayPlayer.Requset) {
        self.playerController.play()
        let response = MaxiPlayer.DisplayPlayPause.Response(playerIsPlaying: self.playerController.isPlaying)
        presenter?.presentPlayPauseButton(response: response)
    }
    
    func pausePlayer(request: MaxiPlayer.DisplayPausePlayer.Request) {
        self.playerController.pause()
        let response = MaxiPlayer.DisplayPlayPause.Response(playerIsPlaying: self.playerController.isPlaying)
        presenter?.presentPlayPauseButton(response: response)
    }
    
    func setFavorite(request: MaxiPlayer.DisplayFavorite.Request) {
        self.isFavorite = !self.isFavorite
        
        guard let song = self.playerController.currentSong else {
            print("<MaxiPlayerInteractor>: <setFavorite>: no current song")
            return
        }
        
        if self.isFavorite {
            self.playerController.playlistController.addSongToPlaylist(song: song, type: .favorites)
        }
        else {
            self.playerController.playlistController.removeSongFromPlaylist(song: song, type: .favorites)
        }
        
        let response = MaxiPlayer.DisplayFavorite.Response(
            isFavorite: self.isFavorite)
        presenter?.presentFavorite(response: response)
    }
    
    func setState(request: MaxiPlayer.DisplayState.Request) {
        guard let item = self.playerController.player.currentItem else {
            print("<MaxiPlayerInteractor>: <setState>: no item in player")
            return
        }
        
        self.setSongData()
        
        let playPauseResponse = MaxiPlayer.DisplayPlayPause.Response(
            playerIsPlaying: self.playerController.isPlaying)
        presenter?.presentPlayPauseButton(response: playPauseResponse)
        
        self.isFavorite = playerController.playlistController.findSongInPlaylist(song: playerController.currentSong, type: .favorites)
        let favoriteResponse = MaxiPlayer.DisplayFavorite.Response(
            isFavorite: self.isFavorite)
        presenter?.presentFavorite(response: favoriteResponse)
        
        guard let progresResponse = self.getProgressResponse(item: item) else {
            print("<MaxiPlayerInteractor>: <setState>: progress response is nil")
            return
        }
        presenter?.presentSongProgress(response: progresResponse)
    }
    
    func setupAlert(request: MaxiPlayer.DisplaySelectPlaylistAlert.Request) {
        let playlists = playerController.playlistController.getOthersPlaylistsNames()
        
        let response = MaxiPlayer.DisplaySelectPlaylistAlert.Response(
            playlistsNames: playlists,
            playlistsTypes: [])
        presenter?.presentSelectPlaylistAlert(response: response)
    }
    
    func forwardPlayer(request: MaxiPlayer.DisplayForward.Request) {
        self.playerController.forward()
        
        let request = MaxiPlayer.DisplayState.Request()
        self.setState(request: request)
    }
    
    func backwardPlayer(request: MaxiPlayer.DisplayBackward.Request) {
        self.playerController.backward()
        
        let request = MaxiPlayer.DisplayState.Request()
        self.setState(request: request)
    }

    func setSongData() {
        guard let song = self.playerController.currentSong else {
            return
        }
        
        let response = MaxiPlayer.DisplaySongData.Response(
            artwork: song.artworkUrl,
            title: song.title,
            author: song.username)
        presenter?.presentSongData(response: response)
    }
    
    
    func configure() {
        isFavorite = false
        
        APISoundCloud.shared.getTrackComments(with: playerController.currentSong.id) { [weak self] (collection) in
            self?.commentsCollection = collection
        }
        
        self.timeObserver = self.playerController.addTimeObserver { [weak self](time) in
            guard let item = PlayerController.shared.player.currentItem else {
                print("<closure>: <MaxiPlayerInteractor>: <configure>: no item in player")
                return
            }
            guard let progresResponse = self?.getProgressResponse(item: item) else {
                print("<MaxiPlayerInteractor>: <setState>: progress response is nil")
                return
            }
            self?.presenter?.presentSongProgress(response: progresResponse)

            guard let commentsResponse = self?.getCommentResponse(item: item) else {
                print("<MaxiPlayerInteractor>: <setState>: comments response is nil")
                return
            }
            self?.presenter?.presentComment(response: commentsResponse)
        }
    }
    
    func getProgressResponse(item: AVPlayerItem) -> MaxiPlayer.DisplayProgress.Response? {
        let currentTime = item.currentTime().seconds
        let duration = item.duration.seconds
        
        if currentTime.isNaN || duration.isNaN {
            print("<closure>: <MaxiPlayerInteractor>: <configure>: NaN was identified")
            return nil
        }
        let response = MaxiPlayer.DisplayProgress.Response(
            currentTime: Float(currentTime),
            duration: Float(duration))
        
        return response
    }
    
    func getCommentResponse(item: AVPlayerItem) -> MaxiPlayer.DisplayComment.Response? {
        guard let collection = self.commentsCollection?.collection else {
            print("<MaxiPlayerInteractor>: <showComment>: no comments")
            return nil
        }
        
        let value =  Int(item.currentTime().seconds)
        
        for comment in collection {
            let timestamp = comment.timestamp / 1000
            
            if Int(value) > timestamp - 1 && value < timestamp + 1 {
                let response = MaxiPlayer.DisplayComment.Response(
                    username: comment.username,
                    message: comment.body)
                
                //print("\(comment.username):\(comment.body)")
                return response
            }
        }
        return nil
    }

    func addSongToThePlaylist(type: PlaylistType) {
        guard let song = self.playerController.currentSong else {
            return
        }
        self.playerController.playlistController.addSongToPlaylist(song: song, type: type)
    }
    
    func deconfigure() {
        self.playerController.removeTimeObserver(observer: timeObserver)
        timeObserver = nil
    }
}
