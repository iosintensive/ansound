//
//  SearchViewController.swift
//  AnSound
//
//  Created by Mac on 27.06.2021.
//

import UIKit
import AVFoundation

class SearchSongsViewController: UIViewController {
    
    //Clean Swift
    //var interactor: SearchSongsInteractor!
    
    //var songCollection: SongCollectionModel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
    }
    
    var playlist: PlaylistModel!
    
    var imagesCollection: [UIImage?]!
    
    @IBOutlet weak var resultsForLabel: UILabel!
    @IBOutlet weak var songTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var MiniPlayerVC: MiniPlayerViewController!
    
    @IBOutlet weak var tabBarPanel: UITabBarItem!

    var placeHolder: UIView!
    
    let searchString = "linkin park"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        miniPlayerInit()
        
        //playlist = MiniPlayerVC.controller.playlistController.currentPlaylist
        
        songTableView.delegate = self
        songTableView.dataSource = self
        
        searchBar.delegate = self
        
        APISoundCloud.shared.delegate = self
        
        //APISoundCloud.shared.searchTracksWith(name: searchString)
        
        resultsForLabel.font = UIFont(name: "Interstate-Bold", size: 20)
        resultsForLabel.text = ""
        
        if playlist.count == 0 {
            showPlaceholder()
        }
    }
    
    func showPlaceholder() {
        let view = UIView(frame: .zero)
        let label = UILabel()
        label.font = UIFont(name: "Interstate-Bold", size: 20)!
        label.textColor = UIColor(named: "MainColor")!
        label.text = "Type something to search"
        label.frame = view.bounds
        
        view.frame = self.songTableView.bounds
        view.backgroundColor = .black
        
        label.translatesAutoresizingMaskIntoConstraints = false
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let cH = NSLayoutConstraint(
            item: label,
            attribute: .centerX,
            relatedBy: .equal,
            toItem: view,
            attribute: .centerX,
            multiplier: 1,
            constant: 0)
        let cV = NSLayoutConstraint(
            item: label,
            attribute: .centerY,
            relatedBy: .equal,
            toItem: view,
            attribute: .centerY,
            multiplier: 1,
            constant: 0)

        let constrH = NSLayoutConstraint(
            item: view,
            attribute: .centerX,
            relatedBy: .equal,
            toItem: self.songTableView,
            attribute: .centerX,
            multiplier: 1,
            constant: 0)
        let constrV = NSLayoutConstraint(
            item: view,
            attribute: .centerY,
            relatedBy: .equal,
            toItem: self.songTableView,
            attribute: .centerY,
            multiplier: 1,
            constant: 0)
        let height = NSLayoutConstraint(
            item: view,
            attribute: .height,
            relatedBy: .equal,
            toItem: self.songTableView,
            attribute: .height,
            multiplier: 1,
            constant: 0)
        let width = NSLayoutConstraint(
            item: view,
            attribute: .width,
            relatedBy: .equal,
            toItem: self.songTableView,
            attribute: .width,
            multiplier: 1,
            constant: 0)
        
        view.addSubview(label)
        view.addConstraints([cH,cV])
        
        placeHolder = view
        self.view.addSubview(view)
        self.view.addConstraints([width, height,constrH, constrV])
        
        
        
        print("collection is empty")
    }
    
    func miniPlayerInit() {
        let mpVC = AppDelegate.miniplayervc
        guard let miniPlayerVC = mpVC else {
            return
        }
        //miniPlayerVC.player = AVAudioPlayer()
        miniPlayerVC.controller = PlayerController()
        let view = miniPlayerVC.view!
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let bottomConstraint = NSLayoutConstraint(
            item: view,
            attribute: .bottom,
            relatedBy: .equal,
            toItem: self.view.safeAreaLayoutGuide,
            attribute: .bottom,
            multiplier: 1,
            constant: 0)
        let leftConstraint = NSLayoutConstraint(
            item: view,
            attribute: .left,
            relatedBy: .equal,
            toItem: self.view,
            attribute: .left,
            multiplier: 1,
            constant: 0)
        let rightConstraint = NSLayoutConstraint(
            item: view,
            attribute: .right,
            relatedBy: .equal,
            toItem: self.view,
            attribute: .right,
            multiplier: 1,
            constant: 0)
        let heightConstraint = NSLayoutConstraint(
            item: view,
            attribute: .height,
            relatedBy: .equal,
            toItem: nil,
            attribute: .height,
            multiplier: 1,
            constant: 45)
        
        miniPlayerVC.view.alpha = 0
        
        self.view.addSubview(miniPlayerVC.view)
        
        self.addChild(miniPlayerVC)
        
        miniPlayerVC.didMove(toParent: self)
        
        self.view.addConstraints([bottomConstraint,leftConstraint,rightConstraint,heightConstraint])
        self.MiniPlayerVC = miniPlayerVC
        
        self.playlist = PlaylistModel()
    }
}

extension SearchSongsViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else {
            return
        }
        if searchBar.text?.count != 0 {
            print("text: \(text)")
            APISoundCloud.shared.searchTracksWith(name: text)
            resultsForLabel.text = "Results for \"\(text)\""
            print("button clicked")
        }
        else {
            print("text is empty")
        }
        //self.placeHolder.alpha = 0
        //self.songTableView.refreshControl = UIRefreshControl()
        //self.songTableView.refreshControl?.beginRefreshing()
        
        self.view.endEditing(true)
    }
}

extension SearchSongsViewController: APISoundCloudDelegate {
    func tracksWasFinded(collection: SongCollectionModel) {
        if !collection.collection.isEmpty {
            DispatchQueue.main.async {
                self.placeHolder.alpha = 0
            }
            playlist.collection = collection
        }
       
        
        imagesCollection = [UIImage?](repeating: nil, count: collection.collection.count)
        
        DispatchQueue.main.async { [weak self] in
            self?.songTableView.reloadData()
            
        }
        
    }
}

extension SearchSongsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SongTableViewCell") as! SongTableViewCell
        
        let index = indexPath.row
        
        if let song = playlist[index] {
            cell.songNameLabel.text = song.title
            cell.authorNameLabel.text = song.username
            
            if let image = imagesCollection[index] {
                cell.songImageView.image = image
                print("SearchViewController: TabelView: Image was loaded from imagesCollection")
            }
            else if let imageUrl = song.artworkUrl {
                cell.songImageView.downloaded(from: imageUrl) { [weak self, index] data in
                    guard let selfView = self else {
                        return
                    }
                    selfView.imagesCollection[index] = UIImage(data: data)
                }
            }
        }
        else {
            print("Error loading cell data")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        MiniPlayerVC.controller.playlistController.selectPlaylist(type: .search)
        
        let currentPlaylist = MiniPlayerVC.controller.playlistController.currentPlaylist
        currentPlaylist?.collection = playlist.collection
        
        
        if let song = currentPlaylist?[indexPath.row, with: .changeCurrentIndex] {
            //print("start loading \(song.title)")
            
            self.MiniPlayerVC.controller.setSong(with: song)
            self.MiniPlayerVC.updateMiniPlayerData()
        }
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor(named: "MainColor")!
        header.textLabel?.font = UIFont(name: "Interstate-Bold", size: 20)!
        //header.backgroundColor = UIColor.black
        header.tintColor = UIColor.clear
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlist.count
    }

}
