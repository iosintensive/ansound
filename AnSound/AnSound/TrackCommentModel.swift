//
//  TrackComment.swift
//  AnSound
//
//  Created by Mac on 13.07.2021.
//

import Foundation

class TrackCommentModel: Decodable {
    let id: Int
    let body: String
    let timestamp: Int
    let username: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case body
        case timestamp
        case user
    }
    enum UserCodingKeys: String, CodingKey {
        case username
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.body = try container.decode(String.self, forKey: .body)
        self.timestamp = try container.decode(Int.self, forKey: .timestamp)
        
        let user = try container.nestedContainer(keyedBy: UserCodingKeys.self, forKey: .user)
        self.username = try user.decode(String.self, forKey: .username)
    }
}

