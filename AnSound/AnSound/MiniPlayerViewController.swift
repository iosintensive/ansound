//
//  PlayerShortViewController.swift
//  AnSound
//
//  Created by Mac on 01.07.2021.
//

import UIKit
//import AVFoundation
import AVKit
import MediaPlayer

protocol MiniPlayerDelegate {
    func updateMiniPlayerData()
}

class MiniPlayerViewController: UIViewController, MiniPlayerDelegate {

    //public var player: AVAudioPlayer!
    public var playlist: PlaylistModel!
    
    public var controller: PlayerController!
    
    public var updater: CADisplayLink!
    
    public var isPlaying: Bool!
    
    public var image: UIImage?
    
    // Main UIProperties
    @IBOutlet weak var songProgressView: UIProgressView!
    @IBOutlet weak var songTitleLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var playStopButton: UIButton!
    
    //static method which helps create an instance
    static func storyboardInstance() -> MiniPlayerViewController? {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: nil)
        return storyboard.instantiateInitialViewController() as? MiniPlayerViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        controller = AppDelegate.playerController
        let timeObserver = controller.addTimeObserver { [weak self] (time) in
            guard let selfCV = self else {
                return
            }
            guard let item = self?.controller.player.currentItem else {
                return
            }
            let progress = Float(item.currentTime().seconds) / Float(item.duration.seconds)
            selfCV.songProgressView.progress = progress
        }
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(miniPlayerDidTouch))
        self.view.addGestureRecognizer(gesture)
        setBlur()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func miniPlayerDidTouch() {
        guard let vc = MaxiPlayerViewController.storyboardInstance() else {
            print("<MiniPlayerViewController>: <miniPlayerDidTouch>: no MaxiPlayerViewController")
            return
        }
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    func setBlur() {
        let blurEffect = UIBlurEffect(style: .systemMaterialDark)

        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        
        blurredEffectView.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = NSLayoutConstraint(
            item: blurredEffectView,
            attribute: .top,
            relatedBy: .equal,
            toItem: view,
            attribute: .top,
            multiplier: 1,
            constant: 0)
        let bottomConstraint = NSLayoutConstraint(
            item: blurredEffectView,
            attribute: .bottom,
            relatedBy: .equal,
            toItem: view,
            attribute: .bottom,
            multiplier: 1,
            constant: 0)
        let leftConstraint = NSLayoutConstraint(
            item: blurredEffectView,
            attribute: .left,
            relatedBy: .equal,
            toItem: view,
            attribute: .left,
            multiplier: 1,
            constant: 0)
        let rightConstraint = NSLayoutConstraint(
            item: blurredEffectView,
            attribute: .right,
            relatedBy: .equal,
            toItem: view,
            attribute: .right,
            multiplier: 1,
            constant: 0)
        
        //blurredEffectView.frame = view.bounds
        view.insertSubview(blurredEffectView, at: 0)
        view.addConstraints([topConstraint,bottomConstraint,leftConstraint,rightConstraint])
    }
    
    func updateMiniPlayerData() {
        guard let song = self.controller.currentSong else {
            return
        }
        self.updatePlayPauseButton()
        
        self.songTitleLabel.text = song.title
        self.authorNameLabel.text = song.username
        
        self.view.alpha = 1
    }
    
    func updatePlayPauseButton() {
        if self.controller.isPlaying {
            playStopButton.setImage(UIImage(systemName: PlayerModels.ButtonNames.pause), for: .normal)
        }
        else {
            playStopButton.setImage(UIImage(systemName: PlayerModels.ButtonNames.play), for: .normal)
        }
    }
    
    func play() {
        self.controller.play()
    }
    func pause() {
        self.controller.pause()
    }
    @IBAction func playStopButtonDidTouch(_ sender: Any) {
        if self.controller.isPlaying {
            self.pause()
        }
        else {
            self.play()
        }
        self.updatePlayPauseButton()
    }
    @IBAction func forwardButtonDidTouch(_ sender: Any) {
        self.controller.forward()
        self.updateMiniPlayerData()
    }
    @IBAction func backwardButtonDidTouch(_ sender: UIButton) {
        self.controller.backward()
        self.updateMiniPlayerData()
    }
}
